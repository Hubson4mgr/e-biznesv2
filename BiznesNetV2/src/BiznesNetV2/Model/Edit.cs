﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BiznesNetV2.Model
{
    public class Edit
    {
        public string entityName { get; set; }
        public string id { get; set; }
        public dynamic newObj { get; set; }
    }
}
