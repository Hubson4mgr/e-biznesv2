﻿myApp.service('commentSvc', ['storageSvc', 'Entity', 'Comment', function (storageSvc, Entity, Comment) {

    var entityName = Entity.Comment;
    
    var svc = angular.extend(this, {
        get: get,
        push: push,
        edit: edit,
    });

    function get(filter) {
        return storageSvc.getEntitiesByName(entityName, filter).then(function (data) {
            if (angular.isArray(data)) {
                return data.map(function (item) { return new Comment(item) });
            }
            else {
                return new Comment(item);
            }
        });
    };

    function edit(id, newObj) {
        return storageSvc.edit(entityName, id, new Comment(newObj)).then(function (data) {
            return new Comment(data);
        });
    };

    function push(newObj) {
        return storageSvc.push(entityName, new Comment(newObj)).then(function (data) {
            return new Comment(data);
        });
    };

}]);