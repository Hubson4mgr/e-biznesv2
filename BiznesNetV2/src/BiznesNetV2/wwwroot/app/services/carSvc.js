﻿myApp.service('carSvc', ['storageSvc', 'Entity', 'Car', function (storageSvc, Entity, Car) {

    var entityName = Entity.Car;
    
    var svc = angular.extend(this, {
        entityName: entityName,
        get: get,
        push: push,
        edit: edit,
        refreshObj: refreshObj
    });

    function get(filter) {
        return storageSvc.getEntitiesByName(entityName, filter).then(function (data) {
            if (angular.isArray(data)) {
                return data.map(function (item) { return new Car(item) });
            }
            else {
                return new Car(item);
            }
        });
    };

    function refreshObj(obj) {
       return svc.get(function (item) { return item.Id = obj.Id }).then(function (response) {
            var car = response[0];
            angular.extend(obj, car);
            return obj;
        });
    };

    function edit(id, newObj) {
        return storageSvc.edit(entityName, id, new Car(newObj)).then(function (data) {
            return new Car(data);
        });
    };

    function push(newObj) {
        return storageSvc.push(entityName, new Car(newObj)).then(function (data) {
            return new Car(data);
        });
    };

}]);