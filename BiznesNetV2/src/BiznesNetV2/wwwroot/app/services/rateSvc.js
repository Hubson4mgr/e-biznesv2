﻿myApp.service('rateSvc', ['storageSvc', 'Entity', 'Rate', function (storageSvc, Entity, Rate) {
    var entityName = Entity.Rate;

    var svc = angular.extend(this, {
        get: get,
        push: push,
        edit: edit,
        rate: rate
    });

    function get(filter) {
        return storageSvc.getEntitiesByName(entityName, filter).then(function (data) {
            if (angular.isArray(data)) {
                return data.map(function (item) { return new Rate(item) });
            }
            else {
                return new Rate(item);
            }
        });
    };

    function rate(userId, carId, rate) {
        return svc.get(function (item) { return item.CarId == carId && item.UserId == userId }).then(function (results) {
            if (results && results[0]) { // then update
                var edited = results[0];
                edited.Rate = rate;
                edited.Date = new Date();

                return svc.edit(edited.Id, edited).then(function (editedRate) {
                    return editedRate;
                });
            }
            else {
                return svc.push({
                    Rate: rate,
                    UserId: userId,
                    CarId: carId,
                    Date: new Date()
                }).then(function (addedRate) {
                    return addedRate;
                });
            }
        });
    };

    function edit(id, newObj) {
        return storageSvc.edit(entityName, id, new Rate(newObj)).then(function (data) {
            return new Rate(data);
        });
    };

    function push(newObj) {
        return storageSvc.push(entityName, new Rate(newObj)).then(function (data) {
            return new Rate(data);
        });
    };

}]);