﻿myApp.service('orderSvc', ['storageSvc', 'Entity', 'Order', 'carSvc', function (storageSvc, Entity, Order, carSvc) {

    var entityName = Entity.Order;

    var svc = angular.extend(this, {
        get: get,
        push: push,
        order: order,
        edit: edit
    });

    function get(filter) {
        return storageSvc.getEntitiesByName(entityName, filter).then(function (data) {
            if (angular.isArray(data)) {
                return data.map(function (item) { return new Order(item) });
            }
            else {
                return new Order(item);
            }
        });
    };

    function push(newObj) {
        return storageSvc.push(entityName, new Order(newObj)).then(function (data) {
            return new Order(data);
        });
    };

    function edit(id, newObj) {
        return storageSvc.edit(entityName, id, newObj).then(function (data) {
            return new Order(data);
        });
    };

    function order(newObj) {
        return push(newObj).then(function (order) {
            return order.joinCar().then(function (joinedOrder) {
                var car = joinedOrder.Car;
                car.Available = car.Available > 1 ? car.Available - 1 : 0;
               return carSvc.edit(car.Id, car).then(function (updatedCar) {
                    if (updatedCar) {
                        order.Car = updatedCar;
                        return order;
                    }
                });
            });
        });
    };

}]);