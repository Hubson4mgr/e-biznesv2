﻿myApp.service('categorySvc', ['storageSvc', 'Entity', 'Category', function (storageSvc, Entity, Category) {

    var entitiyName = Entity.Category;
    
    var svc = angular.extend(this, {
        get: get
    });

    function get(filter) {
        return storageSvc.getEntitiesByName(entitiyName, filter).then(function (data) {
            if (angular.isArray(data)) {
                return data.map(function (item) { return new Category(item) });
            }
            else {
                return new Category(item);
            }
        });
    }

}]);