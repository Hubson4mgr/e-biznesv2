﻿myApp.service('userSvc', ['storageSvc', 'Entity', 'User', '$cookies', 'AppKey', '$crypto', '$q', '$location', function (storageSvc, Entity, User, $cookies, AppKey, $crypto, $q, $location) {

    var entityName = Entity.User;

    var svc = angular.extend(this, {
        get: get,
        push: push,
        login: login,
        isLogin: false,
        isAdmin: false,
        logout: logout,
        checkIsLogin: checkIsLogin,
        getUserId: getUserId,
        getLoggedUser: getLoggedUser,
        putCookie: putCookie
    });

    function getUserId() {
        return parseInt($cookies.get(AppKey.CookieLogin));
    };

    function getLoggedUser() {
        return svc.get(function (item) { return item.Id == svc.getUserId() }).then(function (users) {
            if (users && users[0]) {
                return users[0];
            }
            else {
                return null;
            }
        });
    };

    function get(filter) {
        return storageSvc.getEntitiesByName(entityName, filter).then(function (data) {
            if (angular.isArray(data)) {
                return data.map(function (item) { return new User(item) });
            }
            else {
                return new User(item);
            }
        });
    };

    function push(newObj) {
        return storageSvc.push(entityName, new User(newObj)).then(function (data) {
            return new User(data);
        });
    };

    function login(Email, Password) {
        return svc.get(function (item) { return item.Email == Email; }).then(function (users) {
            var user = users[0];

            var encrypted = $crypto.decrypt(user.Password);

            if (encrypted == Password) {
                $cookies.put(AppKey.CookieLogin, user.Id);
                svc.isLogin = true;
                svc.isAdmin = user.IsAdmin;
                $location.path('/');
                return true;
            }
            else {
                return false;
            }
        });
    };
    function putCookie(userId) {
        $cookies.put(AppKey.CookieLogin, userId);
    };

    function logout() {
        

        svc.get(function (item) { return item.Id = svc.getUserId() }).then(function (users) {
            if (users &&  users[0] != null && users[0].FacebookId != null) {
                Fb.logout();
            }
            svc.isLogin = false;
            svc.isAdmin = false;
            $cookies.remove(AppKey.CookieLogin);
            $location.path('/');
        });
    };

    function checkIsLogin() {
        var cookie = svc.getUserId();
        svc.get(function (item) { return item.Id == cookie }).then(function (response) {
            if (cookie && response && response[0]) {
                svc.isLogin = true;
                svc.isAdmin = response[0].IsAdmin;
            }
        });
    };
}]);

