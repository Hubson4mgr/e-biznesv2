﻿myApp.service('siteSvc', ['$mdDialog', function ($mdDialog) {

    
    var svc = angular.extend(this, {
        openAlert : openAlert
    });

    function openAlert(title, textContent, ok, callback) {
        $mdDialog.show(
              $mdDialog.alert()
                .parent(angular.element(document.querySelector('body')))
                .clickOutsideToClose(true)
                .title(title)
                .textContent(textContent)
                .ariaLabel('Informacja')
                .ok(ok)
            ).then(function () {
                if (callback) {
                    callback();
                }
            });
    };

}]);