﻿myApp.service('storageSvc', ['$http', '$rootScope', 'AppKey', '$templateCache', function ($http, $rootScope, AppKey, $templateCache) {

    var svc = angular.extend(this, {
        getEntitiesByName: getEntitiesByName,
        edit: edit,
        push: push
    });

    var entityPathFormatter = function (EntityName) {
        return "app/storage/" + EntityName + ".json";
    };

    function getEntitiesByName(entityName, filterFunction) {
        $templateCache.remove(entityPathFormatter(entityName));
        return $http.get(entityPathFormatter(entityName), { cache: false }).then(function (response) {
            if (filterFunction && angular.isArray(response.data)) {
                return response.data.filter(filterFunction);
            }
            else {
                return response.data;
            }
        });
    };

    function edit(entityName, id, newObj) {
        $templateCache.remove('app/Api/EditEntity');

        return $http.post('app/Api/EditEntity', {
            entityName: entityName,
            id: id,
            newObj: newObj
        }).then(function (response) {
            $rootScope.$broadcast(AppKey.Edit + entityName,
            {
                newObj: response.data,
                id: id
            });
            return response.data;
        });
    };


    function push(entityName, newObj) {
        $templateCache.remove('app/Api/PushEntity');

        return $http.post('app/Api/PushEntity', {
            entityName: entityName,
            newObj: newObj
        }).then(function (response) {
            return response.data;
        });
    };

}]);