﻿myApp.directive('carCard', ['$mdDialog', 'userSvc', 'siteSvc', '$location', function ($mdDialog, userSvc, siteSvc, $location) {
    return {
        scope: {
            car: "="
        },
        restrict: "AE",
        templateUrl: "/app/templates/CarCard.html",
        controller: function ($scope) {

            function DialogController($scope, $mdDialog, car) {
                $scope.car = car;

                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.answer = function (answer) {
                    $mdDialog.hide(answer);
                };
            }

            function openHireDialog(ev) {
                if (userSvc.isLogin == false)
                    siteSvc.openAlert('Logowanie', 'Prosze się zalogować lub zarejestrować', 'Zamknij');
                else
                    $mdDialog.show({
                        controller: DialogController,
                        templateUrl: '/app/templates/Hire.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: true,
                        fullscreen: false,
                        locals: { car: $scope.car }
                    });
            };

            $scope.openHireDialog = openHireDialog;

            $scope.goCard = function (car) {
                $location.path('/Car/FullCard/' + car.Id);
            };
          
        }
    };
}]);