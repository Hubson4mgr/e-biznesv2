﻿myApp.factory('Order', ['carSvc', 'userSvc', function (carSvc, userSvc) {

    var model = {
        Id: null,
        UserId: null,
        CarId: null,
        DateFrom: null,
        DateTo: null,
        DaysCount: null,
        Amount: null,
        IsReturned: null,
        IsPaid: null

        //User: null, // Order.UserId => User.Id
        //Car: null // Order.CarId => Car.Id
    };

    var order = function (obj) {
        var data = angular.copy(angular.extend(model, obj));

        angular.extend(this, data);
    };

    order.prototype.joinCar = function () {
        var data = this;
        return carSvc.get(function (car) {
            return car.Id == data.CarId;
        }).then(function (cars) {
            data.Car = cars[0];
            return data;
        });
    };

    order.prototype.joinUser = function () {
        var data = this;
        return userSvc.get(function (car) {
            return car.Id == data.UserId;
        }).then(function (users) {
            data.User = users[0];
            return data;
        });
    };

    return order;
}]);