﻿myApp.factory('User', [function () {

    var model = {
        Id: null,
        Name: null,
        Password: null,
        IsAdmin: false,
        Email: null,
        FacebookId: null,
        Age: 18
    };

    var user = function (obj) {
       angular.extend(this, model, obj);

    };

    return user;
}]);