﻿myApp.factory('Rate', [function () {

    var model = {
        Id: null,
        UserId: null,
        CarId: null,
        Rate: null,
        Date: null

        //User: null, // Rate.UserId => User.Id
        //Car: null // Rate.CarId => Car.Id
    };

    var rate = function (obj) {
        angular.extend(this, model, obj);
    };

    rate.prototype.joinCar = function () {
        var data = this;
        return carSvc.get(function (car) {
            return car.Id == data.CarId;
        }).then(function (cars) {
            data.Car = cars[0];
            return data;
        });
    };

    rate.prototype.joinUser = function () {
        var data = this;
        return userSvc.get(function (car) {
            return car.Id == data.UserId;
        }).then(function (users) {
            data.User = users[0];
            return data;
        });
    };

    return rate;
}]);