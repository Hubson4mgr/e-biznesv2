﻿myApp.factory('Comment', ['userSvc', function (userSvc) {

    var model = {
        Id: null,
        Comment: null,
        Date: null,
        UserId: null,
        CarId: null,
        IsDeleted: false
        //User: null // Commen.UserId => User.Id
    };

    var comment = function (obj) {
        angular.extend(this, model, obj);
    };

    comment.prototype.joinUser = function () {
        var data = this;
        return userSvc.get(function (user) {
            return user.Id == data.UserId;
        }).then(function (users) {
            data.User = users[0];
            return data;
        });
    };

    return comment;
}]);