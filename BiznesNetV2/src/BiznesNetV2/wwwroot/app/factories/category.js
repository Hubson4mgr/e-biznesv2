﻿myApp.factory('Category', ['carSvc', function (carSvc) {
    var model = {
        Id: null,
        Name: null,
        Cars: [] //Category.id => Car.CategoryIds
    };

    var category = function (obj) {
        angular.extend(this, model, obj);
    };

    category.prototype.joinCars = function () {
        var data = this;

        return carSvc.get(function (car) {
            return car.CategoryIds.indexOf(data.Id) > -1
        }).then(function (cars) {
            data.Cars = cars;
            return data;
        });
    }

    return category;
}]);