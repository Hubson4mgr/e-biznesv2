﻿myApp.factory('Car', [function () {

    var model = {
        Id: null,
        Name: null,
        Amount: null,
        Description: null,
        Location: null,
        AvatarPath: null,
        Count: null,
        Available: null,
        IsDeleted: false,

        UserId: null, // owner
        CategoryIds: []
    };

    var car = function (obj) {
        angular.extend(this, model, obj);
    };

    return car;
}]);