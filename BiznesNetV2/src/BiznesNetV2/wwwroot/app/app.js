﻿"use strict";

var isDemoVersion = true;

var myApp = angular.module('myApp',
    ['ngRoute', 'ngMaterial', 'ngCookies', 'mdo-angular-cryptography', 'ngFileUpload', 'ngRateIt', 'chart.js']);

angular.module('myApp').config(['$cryptoProvider', 'AppKey', function ($cryptoProvider, AppKey) {
    $cryptoProvider.setCryptographyKey(AppKey.CryptoKey);
}]);

angular.module('myApp')
    .config(['$httpProvider', '$routeProvider', '$locationProvider', function ($httpProvider, $routeProvider, $locationProvider) {
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        $routeProvider
        .when('/Orders', {
            templateUrl: function (params) {
                return "/app/templates/OrdersList.html";
            }
        })
        .when('/Contact', {
            templateUrl: function (params) {
                return "/app/templates/Contact.html";
            }
        })
        .when('/Analytics', {
            templateUrl: function (params) {
                return "/app/templates/Analytics.html";
            }
        })
         .when('/Features', {
             templateUrl: function (params) {
                 return "/app/templates/Features.html";
             }
         })
         .when('/Regulations', {
             templateUrl: function (params) {
                 return "/app/templates/Regulations.html";
             }
         })
        .when('/AboutUs', {
            templateUrl: function (params) {
                return "/app/templates/AboutUs.html";
            }
        })
        .when('/Home', {
            templateUrl: function (params) {
                return "/app/home/index";
            }
        })
        .when('/Cars/:categoryName', {
            templateUrl: "/app/templates/Cars.html",
            controller: "carsListCtrl",
            controllerAs: "clc"
        })
        .when('/Car/FullCard/:carId', {
            templateUrl: "/app/templates/FullCarCard.html",
            controller: "fullCarCardCtrl",
            controllerAs: "fcc"
        })
        .when('/:controller/:action', {
            resolve: {
                message: function () {
                }
            },
            templateUrl: function (params) {
                var validNames = ['controller', 'action'];
                var queryString = '';
                for (var param in params) {
                    if (validNames.indexOf(param) >= 0) {
                        continue;
                    }
                    if (queryString === '') {
                        queryString = '?' + param + '=' + params[param];
                        continue;
                    }
                    queryString += '&' + param + '=' + params[param];
                }

                return '/app/' + params.controller + '/' + params.action + queryString;
            }
        })
        .when('/:controller/:action/*parameters', {
            templateUrl: function (params) {
                return '/app/' + params.controller + '/' + params.action + '/' + params.parameters;
            }
        })
        .when("/:controller", {
            templateUrl: function (params) {
                return '/app/' + params.controller + '/index'
            }
        })
        .otherwise({ redirectTo: '/home/index' });


        $locationProvider.html5Mode(true);
    }])
    .run(['$rootScope', '$templateCache',
        function ($rootScope, $templateCache) {

            $rootScope.$on('$routeChangeStart', function (event, current, previous) {
                $rootScope.routeIsLoading = true;

                if (typeof (previous) !== 'undefined') {
                    $templateCache.remove(previous.templateUrl);
                }
            });

            $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
                $rootScope.routeIsLoading = false;
            });

            $rootScope.$on("$routeChangeError", function (event, current, previous, rejection) {
                if (typeof rejection === 'undefined' && !rejection.authenticated) {
                    $location.path('/account/login');
                }
            });

        }]);

