"use strict";

var isDemoVersion = true;

var myApp = angular.module('myApp',
    ['ngRoute', 'ngMaterial', 'ngCookies', 'mdo-angular-cryptography', 'ngFileUpload', 'ngRateIt', 'chart.js']);

angular.module('myApp').config(['$cryptoProvider', 'AppKey', function ($cryptoProvider, AppKey) {
    $cryptoProvider.setCryptographyKey(AppKey.CryptoKey);
}]);

angular.module('myApp')
    .config(['$httpProvider', '$routeProvider', '$locationProvider', function ($httpProvider, $routeProvider, $locationProvider) {
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        $routeProvider
        .when('/Orders', {
            templateUrl: function (params) {
                return "/app/templates/OrdersList.html";
            }
        })
        .when('/Contact', {
            templateUrl: function (params) {
                return "/app/templates/Contact.html";
            }
        })
        .when('/Analytics', {
            templateUrl: function (params) {
                return "/app/templates/Analytics.html";
            }
        })
         .when('/Features', {
             templateUrl: function (params) {
                 return "/app/templates/Features.html";
             }
         })
         .when('/Regulations', {
             templateUrl: function (params) {
                 return "/app/templates/Regulations.html";
             }
         })
        .when('/AboutUs', {
            templateUrl: function (params) {
                return "/app/templates/AboutUs.html";
            }
        })
        .when('/Home', {
            templateUrl: function (params) {
                return "/app/home/index";
            }
        })
        .when('/Cars/:categoryName', {
            templateUrl: "/app/templates/Cars.html",
            controller: "carsListCtrl",
            controllerAs: "clc"
        })
        .when('/Car/FullCard/:carId', {
            templateUrl: "/app/templates/FullCarCard.html",
            controller: "fullCarCardCtrl",
            controllerAs: "fcc"
        })
        .when('/:controller/:action', {
            resolve: {
                message: function () {
                }
            },
            templateUrl: function (params) {
                var validNames = ['controller', 'action'];
                var queryString = '';
                for (var param in params) {
                    if (validNames.indexOf(param) >= 0) {
                        continue;
                    }
                    if (queryString === '') {
                        queryString = '?' + param + '=' + params[param];
                        continue;
                    }
                    queryString += '&' + param + '=' + params[param];
                }

                return '/app/' + params.controller + '/' + params.action + queryString;
            }
        })
        .when('/:controller/:action/*parameters', {
            templateUrl: function (params) {
                return '/app/' + params.controller + '/' + params.action + '/' + params.parameters;
            }
        })
        .when("/:controller", {
            templateUrl: function (params) {
                return '/app/' + params.controller + '/index'
            }
        })
        .otherwise({ redirectTo: '/home/index' });


        $locationProvider.html5Mode(true);
    }])
    .run(['$rootScope', '$templateCache',
        function ($rootScope, $templateCache) {

            $rootScope.$on('$routeChangeStart', function (event, current, previous) {
                $rootScope.routeIsLoading = true;

                if (typeof (previous) !== 'undefined') {
                    $templateCache.remove(previous.templateUrl);
                }
            });

            $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
                $rootScope.routeIsLoading = false;
            });

            $rootScope.$on("$routeChangeError", function (event, current, previous, rejection) {
                if (typeof rejection === 'undefined' && !rejection.authenticated) {
                    $location.path('/account/login');
                }
            });

        }]);


myApp.controller('analyticCtrl', ['carSvc', '$window', 'orderSvc', 'userSvc', 'commentSvc', '$timeout', 'categorySvc', '$q', function (carSvc, $window, orderSvc, userSvc, commentSvc, $timeout, categorySvc, $q) {

    var ctr = angular.extend(this, {
        init: init,
        ordersPerMonth: {},
        topCategories: {},
        topDays: {}
    });



    function setupOrdersPerMonths() {
        orderSvc.get().then(function (orders) {
            var grouped = _.groupBy(orders, function (order) { return new Date(order.DateFrom).getMonth() + 1; });

            var data = [];

            for (var i = 1; i <= 12; i++) {
                if (grouped[i]) {
                    data.push(grouped[i].length);
                }
                else {
                    data.push(0);
                }
            }


            var model = {
                labels: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
                series: ['Liczba zamówień'],
                data: [
                        data
                ],
                onClick: angular.noop,
            };

            ctr.ordersPerMonth = model;
            ctr.ordersPerMonth.isInited = true;
        });
    };

    function setupTopCategories() {
        categorySvc.get().then(function (categories) {
            orderSvc.get().then(function (orders) {
                $q.all(orders.map(function (item) { return item.joinCar() })).then(function () {
                    var orderedAllCars = orders.map(function (order) { return order.Car });
                    var merged = [].concat.apply([], orderedAllCars.map(function (car) { return car.CategoryIds }));
                    var grouped = _.groupBy(merged, function (categoryId) { return categoryId });
                    var data = [];

                    categories.forEach(function (category) {
                        if (grouped[category.Id]) {
                            data.push(grouped[category.Id].length);
                        }
                        else {
                            data.push(0);
                        }
                    });

                    var model = {
                        labels: categories.map(function (item) { return item.Name }),
                        series: ['Liczba zamówień'],
                        data: [
                                data
                        ],
                        onClick: angular.noop,
                    };
                    ctr.topCategories = model;
                    ctr.topCategories.isInited = true;
                });
            });
        });
    };

    function setupTopDays() {

        orderSvc.get().then(function (orders) {
            var days = orders.map(function (item) {
                return item.DaysCount;
            })
            .sort(function (a, b) {
                return a - b;
            });

            var grouped = _.groupBy(days, function (item) { return item });
            var data = [];
            var labels = [];
            for (var item in grouped) {
                data.push(grouped[item].length)
                labels.push(item + " dni");
            }
            var model = {
                labels: labels,
                data: data,
                onClick: angular.noop,
            };
            ctr.topDays = model;
            ctr.topDays.isInited = true;
        });

    };

    function init() {
        $timeout(function () {
            setupOrdersPerMonths();
            setupTopCategories();
            setupTopDays();
        }, 100);

    };

}]);
myApp.controller('carCtrl', ['carSvc', '$mdDialog', '$scope', 'Upload', 'categorySvc', '$cookies', 'AppKey', '$timeout','$rootScope',
    function (carSvc, $mdDialog, $scope, Upload, categorySvc, $cookies, AppKey, $timeout,$rootScope) {

    var carModel = {
        Name: null,
        Description: null,
        Amount: 0.0,
        Location: null,
        AvatarPath: null,
        UserId: parseInt($cookies.get(AppKey.CookieLogin)),
        Count: 1,
        Available: 1,
        CategoryIds: [],
    };


    var ctr = angular.extend(this, {
        init: init,
        carModel: carModel,
        carAddSubmit: carAddSubmit,
        upload: upload,
        selectedCategories: null,
        categories: []
    });



    function carAddSubmit() {
        carModel.CategoryIds = ctr.selectedCategories.map(function (item) { return JSON.parse(item).Id });
        carSvc.push(carModel).then(function (car) {
            if (car) {
                $mdDialog.cancel();
                $rootScope.$broadcast('reload-car-list', true);
            }
        });
    };

    function upload(file) {
        if (file) {
            file.upload = Upload.upload({
                url: '/app/Api/UploadFile',
                data: { file: file }
            }).then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                    carModel.AvatarPath = response.data.filePath;
                });
            });
        }

    };
    
    function setupCategories() {
        categorySvc.get().then(function (categories) {
            ctr.categories = categories;
        });
    };


    function init() {
        setupCategories();
    };
    
}]);
myApp.controller('carsListCtrl', ['$routeParams', 'categorySvc', '$scope', 'carSvc', 'AppKey', function ($routeParams, categorySvc, $scope, carSvc, AppKey) {
    var categoryName = $routeParams.categoryName;

    var ctr = angular.extend(this, {
        init: init,
        category: categoryName,
        cars: [],
        getCars: getCars
    });

    function getCars(search) {
        if (search) {
            return ctr.cars.filter(function (car) { return car.Name.toLowerCase().indexOf(search.toLowerCase()) !== -1 || car.Description.toLowerCase().indexOf(search.toLowerCase()) !== -1 || car.Location.toLowerCase().indexOf(search.toLowerCase()) !== -1 });
        }
        return ctr.cars;
    }

    function loadCarsByCategoryName(categoryName) {
        return categorySvc.get(function (item) { return item.Name == categoryName }).then(function (data) {
            var categoryObj = data[0];
            return categoryObj.joinCars().then(function (category) {
                return category.Cars.filter(function(car){return car.IsDeleted!= true});
            });

        });
    };

    $scope.$on('reload-car-list', init);
    $scope.$on(AppKey.Edit + carSvc.entityName, function (eventParam) {
        init();
    });

    function init() {
        if (categoryName) {
            loadCarsByCategoryName(categoryName).then(function (cars) { ctr.cars = cars });
        }
    };

}]);
myApp.controller('categoryCtrl', ['categorySvc', '$routeParams', function (categorySvc, $routeParams) {

    var ctr = angular.extend(this, {
        init: init,
        categories: categories,
        isActive: isActive
    });

    var categories = [];

    function loadCategories() {
        categorySvc.get().then(function (data) {
            ctr.categories = data;
        });
    };

    function isActive(categoryName) {
        return $routeParams.categoryName == categoryName;
    };

    function init() {
        loadCategories();
    };
    
}]);
myApp.controller('contactUs', ['$scope', function ( $scope) {

    var ctr = angular.extend(this, {
        init: init
    });

         

    function init() {
    };

}]);
myApp.controller('facebookCtrl', ['$scope', '$cookies', 'userSvc', '$mdDialog', '$location',
function ($scope, $cookies, userSvc, $mdDialog, $location) {

    var ctr = angular.extend(this, {
        login: login,

    });


    function registerSubmit(name, email, facebookId) {
        return userSvc.push({
            Name: name,
            Email: email,
            FacebookId: facebookId,
            Age: 18
        })
    };


    function login() {
        FB.login(function (response) {
            FB.api('/me', 'GET', { fields: 'id, name, first_name, email' }, function (response) {
                if (response.id) {
                    userSvc.get(function (item) { return item.FacebookId == response.id }).then(function (users) {
                        if (!(users && users[0])) {
                            registerSubmit(response.name, response.email, response.id).then(function (addedUser) {
                                userSvc.putCookie(addedUser.Id);
                                userSvc.isLogin = true;
                                $mdDialog.cancel();
                                $location.path('/');
                            });
                        }
                        else {
                            userSvc.putCookie(users[0].Id);
                            userSvc.isLogin = true;
                            $mdDialog.cancel();
                            $location.path('/');
                        }
                    });
                }
            });
        }, { scope: 'email' });


        //FB.login(function (response) {
        //    debugger;
        //});
    }

}]);
myApp.controller('fullCarCardCtrl', ['carSvc', '$window', '$scope', '$routeParams', '$rootScope', 'userSvc', 'commentSvc', '$q', '$timeout', 'siteSvc','$location',
function (carSvc, $window, $scope, $routeParams, $rootScope, userSvc, commentSvc, $q, $timeout, siteSvc, $location) {
    var carId = $routeParams.carId;

    var ctr = angular.extend(this, {
        init: init,
        goBack: goBack,
        car: null,
        isCarLoaded: false,
        comments: [],
        getComments: getComments,
        addComment: addComment,
        removeCar: removeCar,
        removeComment: removeComment,
        isUserComment: isUserComment
    });
    var nowIsAddingComment = false;
    function goBack() {
        $rootScope.back();
    };

    function setupCar() {
        return carSvc.get(function (item) { return item.Id == carId }).then(function (cars) {
            var car = cars[0];
            if (car) {
                ctr.car = car;
            }
            ctr.isCarLoaded = true;

            return car;
        });
    };

    function setupComments() {
        return commentSvc.get(function (item) { return item.CarId == ctr.car.Id })
                    .then(function (comments) {
                        ctr.comments = comments;
                        if (ctr.comments) {
                            var joinPromises = ctr.comments.map(function (item) { return item.joinUser() });

                            return $q.all(joinPromises).then(function () {
                                return comments.filter(function (com) { return com.IsDeleted != true});
                            });
                        }
                        else {
                            return [];
                        }
                    });
    };

    function getComments(filter) {
        return nowIsAddingComment == false ? ctr.comments.filter(function (item) {
            return (item.Comment.toLowerCase().indexOf(filter.toLowerCase()) !== -1 ||
                   item.User.Name.toLowerCase().indexOf(filter.toLowerCase()) !== -1) && item.IsDeleted != true
        }) : []
    };

    function addComment() {
        ctr.commentForm.$setSubmitted();
        if (!ctr.commentForm.$valid) return ctr.commentForm.$valid;
        commentSvc.push({
            CarId: ctr.car.Id,
            UserId: userSvc.getUserId(),
            Comment: ctr.comment,
            Date: new Date()
        }).then(function () {
            nowIsAddingComment = true;
            setupComments().then(function () {
                ctr.comment = "";
                nowIsAddingComment = false;
            });
        })
    };

    function removeCar() {
        ctr.car.IsDeleted = true;
        carSvc.edit(ctr.car.Id, ctr.car).then(function () {
            
            siteSvc.openAlert('', 'Auto zostało usunięte', 'Ok', function () { $location.path('/'); });
        });
    };

    function removeComment(comment) {
        comment.IsDeleted = true;
        commentSvc.edit(comment.Id, comment).then(function () {
            siteSvc.openAlert('', 'Komentarz został usunięty', 'Ok', function () {
                setupComments();
            });
        });
    };

    function isUserComment(comment) {
        return userSvc.getUserId() == comment.UserId;
    };

    function init() {
        $window.scrollTo(0, 0);
        setupCar().then(setupComments);
    };

}]);
myApp.controller('hireCarCtrl', ['orderSvc', '$cookies', 'AppKey', '$mdDialog','carSvc','siteSvc','$rootScope',
    function (orderSvc, $cookies, AppKey, $mdDialog, carSvc, siteSvc, $rootScope) {

        var hireModel = {
            DateFrom: null,
            DateTo: null,
            CarId: null,
            UserId: null,
            DaysCount: null,
            Amount: null,
            IsReturned: false
        };

        var ctr = angular.extend(this, {
            init: init,
            hireModel: hireModel,
            hireSubmit: hireSubmit,
            calculatedAmount: calculatedAmount,
            car: null,
        });

        function calculateDays(dateFrom, dateTo) {
            var oneDay = 24 * 60 * 60 * 1000;
            return Math.round(Math.abs((dateFrom.getTime() - dateTo.getTime()) / (oneDay)));
        };

        function calculatedAmount() {
            if (!(ctr.hireModel.DateFrom && ctr.hireModel.DateTo)) return null;

            var days = calculateDays(ctr.hireModel.DateFrom, ctr.hireModel.DateTo);
            var dayAmount = ctr.car.Amount;
            ctr.hireModel.DaysCount = days;
            ctr.hireModel.Amount = days * dayAmount;

            return ctr.hireModel.Amount;
        }

        function hireSubmit() {
            ctr.hireForm.$setSubmitted();
            if (!ctr.hireForm.$valid) return ctr.hireForm.$valid;
            if (ctr.car.Available == 0) {
                siteSvc.showAlert('Brak samochodów', 'Prosimy spróbować w późniejszym terminie', 'Zamknij');
                return;
            } 

            ctr.hireModel.Amount = calculatedAmount(ctr.hireModel.DaysCount, ctr.car.Amount);

            orderSvc.order({
                UserId: ctr.hireModel.UserId,
                CarId: ctr.hireModel.CarId,
                DateFrom: ctr.hireModel.DateFrom,
                DateTo: ctr.hireModel.DateTo,
                DaysCount: ctr.hireModel.DaysCount,
                Amount: calculatedAmount(),
                IsReturned: ctr.hireModel.IsReturned,
                IsPaid: false
            }).then(function (data) {
                if (data) {
                    carSvc.refreshObj(data.Car).then(function () {
                        angular.extend(ctr.car, data.Car);
                    });
                   

                    $mdDialog.cancel();
                }
            });
        };

        function init(car) {
            ctr.car = car;
            ctr.hireModel.CarId = car.Id;
            ctr.hireModel.UserId = parseInt($cookies.get(AppKey.CookieLogin));
        };

    }]);
myApp.controller('orderListCtrl', ['orderSvc', 'siteSvc', 'userSvc', '$q', '$mdDialog', 'carSvc', function (orderSvc, siteSvc, userSvc, $q, $mdDialog, carSvc) {


    var ctr = angular.extend(this, {
        init: init,
        orders: [],
        getFilteredOrders: getFilteredOrders,
        tryToDate: tryToDate,
        payOpen: payOpen
    });

    function DialogController($scope, $mdDialog, order) {
        $scope.order = order;

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.answer = function (answer) {
            $mdDialog.hide(answer);
        };

        $scope.pay = function (order) {
            if (userSvc.isAdmin == true) {
                return orderSvc.edit(order.Id, { IsPaid: true }).then(function (editedOrder) {
                    order.IsPaid = editedOrder.IsPaid;

                    return carSvc.get(function (item) { return item.Id == editedOrder.CarId })
                                  .then(function (cars) {
                                      var car = cars[0];
                                      car.Available = car.Available + 1;
                                      return carSvc.edit(car.Id, car).then(function (editedCar) {
                                          $mdDialog.cancel();
                                      });
                                  });
                });
            }
            else {
                siteSvc.openAlert("Płatność", "Dla bezpieczeństwa funkcjonalność jest zablokowama, system działa na potrzeby projektu zaliczeniowego.", "zamknij");
            }
        };
    }

    function payOpen(ev, order) {
        if (userSvc.isLogin == false)
            siteSvc.openAlert('Logowanie', 'Prosze się zalogować lub zarejestrować', 'Zamknij');
        else
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/templates/PayDialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: false,
                locals: { order: order }
            });
    };

    function tryToDate(order) {
        return (new Date(order.DateTo)) <= new Date() && order.IsPaid == false ? true : false;
    };

    function getFilteredOrders(search) {
        if (search) {

            return ctr.orders.filter(function (order) { return order.Car.Name.toLowerCase().indexOf(search.toLowerCase()) !== -1 || order.Car.Description.toLowerCase().indexOf(search.toLowerCase()) !== -1 || order.Car.Location.toLowerCase().indexOf(search.toLowerCase()) !== -1 });
        }
        return ctr.orders;
    };

    function setupOrdersList() {
        return orderSvc.get(function (item) { return userSvc.isAdmin == false ? item.UserId == userSvc.getUserId() : true }).then(function (orders) {
            var joinPromises = [];
            if (orders) {
                orders.forEach(function (order) { joinPromises.push(order.joinCar()) });

                $q.all(joinPromises).then(function (joinedOrders) {
                    ctr.orders = joinedOrders;
                });
            }
        });
    };

    function init() {
        setupOrdersList()
    };

}]);
myApp.controller('rateCtrl', ['userSvc', '$cookies', 'AppKey', 'rateSvc', '$scope',
    function (userSvc, $cookies, AppKey, rateSvc, $scope) {
        $scope.model = null;
        $scope.userSvc = userSvc;

        var ctr = angular.extend(this, {
            init: init,
            isReadOnlyRate: isReadOnlyRate,
            car: null
        });

        function isReadOnlyRate() {
            return $cookies.get(AppKey.CookieLogin) == null;
        };

        function setupWatch() {
            $scope.$watch('model', function (newVal, oldVal) {
                var userId = parseInt($cookies.get(AppKey.CookieLogin));
                if (newVal != oldVal && userId && oldVal != null) {
                    rateSvc.rate(userId, ctr.car.Id, newVal);
                }
            });
        }
      
        function setupRate() {
            var userId = parseInt($cookies.get(AppKey.CookieLogin));
            if (userId) {
                rateSvc.get(function (item) { return item.CarId == ctr.car.Id && item.UserId == userId }).then(function (results) {
                    if (results && results[0]) {
                        $scope.model = results[0].Rate;
                    } else {
                        $scope.model = 0;
                    }
                });
            }
            else {
                rateSvc.get(function (item) { return item.CarId == ctr.car.Id }).then(function (results) {
                    if (results) {
                        var sum = 0;
                        for (var item in results) {
                            sum = sum + results[item].Rate;
                        }
                        $scope.model = sum / results.length;
                    }
                    else {
                        $scope.model = 0;
                    }
                });
            }
        }

        function init(car) {
            ctr.car = car;
            setupRate();
            setupWatch();
        };

        $scope.$watch('userSvc.isLogin', function (newVal, oldVal) {
            setupRate();
        });

    }]);
myApp.controller('siteCtrl', ['userSvc', '$mdDialog', 'carSvc', '$cookies', 'AppKey', '$scope', '$location', '$rootScope','$timeout',
    function (userSvc, $mdDialog, carSvc, $cookies, AppKey, $scope, $location, $rootScope, $timeout) {
        var site = angular.extend(this, {
            init: init,
            userSvc: userSvc,
            loginOpen: loginOpen,
            registerOpen: registerOpen,
            logout: logout,
            carAddOpen: carAddOpen,
            top3Cars: [],
            getNewsletterCookie: getNewsletterCookie,
            saveNewsletter: saveNewsletter,
            textSearch: "",
            userName: ""
        });
        function getNewsletterCookie() {
            return $cookies.get(AppKey.Newsletter) || false;
        };
        $scope.userSvc = userSvc;

        $scope.$watch('userSvc.isLogin', function (newVal, oldVal) {
            userSvc.getLoggedUser().then(function (user) {
                site.userName = user ? user.Name : '';
            });
        });

        function saveNewsletter() {
            site.newsletterForm.$setSubmitted();
            if (!site.newsletterForm.$valid) return ctr.hireForm.$valid;
            $cookies.put(AppKey.Newsletter, true);
        };

        function DialogController($scope, $mdDialog) {
            $scope.hide = function () {
                $mdDialog.hide();
            };

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        }

        function loginOpen(ev) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/templates/Login.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: false
            })
        };

        function registerOpen(ev) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/templates/Register.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: false
            })
        };

        function carAddOpen(ev) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/templates/AddCar.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: false
            });
        };


        function logout() {
            userSvc.logout();
        };

        function setupTop3Cars() {
            return carSvc.get().then(function (data) {
                site.top3Cars = data.filter(function(car){return car.IsDeleted != true}).slice(0, 3);
            });
        };

        var history = [];

        $rootScope.$on('$routeChangeSuccess', function () {
            history.push($location.$$path);
        });

        $rootScope.back = function () {
            var prevUrl = history.length > 1 ? history.splice(-2)[0] : "/";
            $location.path(prevUrl);
        };

        function fbInit() {
            $timeout(function () {
                FB.getLoginStatus(function (response) {
                    if (response.status == 'connected') {
                        var userFbId = response.authResponse.userID;

                        userSvc.get(function (item) { return item.FacebookId == userFbId; }).then(function (users) {
                            if (users && users[0]) {
                                userSvc.putCookie(users[0].Id);
                                userSvc.isLogin = true;
                            }
                        });
                    }
                });
            }, 400);
        };

        function init() {
            userSvc.checkIsLogin();
            setupTop3Cars();
            fbInit();
        };

    }]);
myApp.controller('toolbarCtrl', ['userSvc', function ($routeParams, userSvc) {
    var categoryName = $routeParams.categoryName;

    var ctr = angular.extend(this, {
        init: init,
        isOpen: false,
        count: 0,
        selectedDirection: 'left'
    });


    function init() {

    };

}]);
myApp.controller('userCtrl', ['userSvc', '$crypto', '$mdDialog', function (userSvc, $crypto, $mdDialog) {

    var registerModel = {
        Name : null,
        Email : null,
        Password: null,
        Age: 18
    };

    var loginModel = {
        Email: null,
        Password: null
    };

    var ctr = angular.extend(this, {
        init: init,
        registerModel: registerModel,
        loginModel: loginModel,
        registerSubmit: registerSubmit,
        loginSubmit: loginSubmit
    });

    function registerSubmit() {
        ctr.registerForm.$setSubmitted();
        if (!ctr.registerForm.$valid) return ctr.registerForm.$valid

        userSvc.push({
            Name: ctr.registerModel.Name,
            Email: ctr.registerModel.Email,
            Password: $crypto.encrypt(ctr.registerModel.Password),
            Age: ctr.registerModel.Age
        }).then(function (user) {
            userSvc.login(user.Email, ctr.registerModel.Password).then(function (isSuccess) {
                if (isSuccess == true) {
                    $mdDialog.cancel();
                }
            });
        });
    };

    function loginSubmit() {
        ctr.loginForm.$setSubmitted();
        if (!ctr.loginForm.$valid) return ctr.loginForm.$valid

        userSvc.login(ctr.loginModel.Email, ctr.loginModel.Password).then(function (isSuccess) {
            if (isSuccess == true) {
                $mdDialog.cancel();
            }
        });
    };

    function init() {
        loadCategories();
    };
    
}]);
myApp.directive('carCard', ['$mdDialog', 'userSvc', 'siteSvc', '$location', function ($mdDialog, userSvc, siteSvc, $location) {
    return {
        scope: {
            car: "="
        },
        restrict: "AE",
        templateUrl: "/app/templates/CarCard.html",
        controller: function ($scope) {

            function DialogController($scope, $mdDialog, car) {
                $scope.car = car;

                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.answer = function (answer) {
                    $mdDialog.hide(answer);
                };
            }

            function openHireDialog(ev) {
                if (userSvc.isLogin == false)
                    siteSvc.openAlert('Logowanie', 'Prosze się zalogować lub zarejestrować', 'Zamknij');
                else
                    $mdDialog.show({
                        controller: DialogController,
                        templateUrl: '/app/templates/Hire.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: true,
                        fullscreen: false,
                        locals: { car: $scope.car }
                    });
            };

            $scope.openHireDialog = openHireDialog;

            $scope.goCard = function (car) {
                $location.path('/Car/FullCard/' + car.Id);
            };
          
        }
    };
}]);
myApp.service('carSvc', ['storageSvc', 'Entity', 'Car', function (storageSvc, Entity, Car) {

    var entityName = Entity.Car;
    
    var svc = angular.extend(this, {
        entityName: entityName,
        get: get,
        push: push,
        edit: edit,
        refreshObj: refreshObj
    });

    function get(filter) {
        return storageSvc.getEntitiesByName(entityName, filter).then(function (data) {
            if (angular.isArray(data)) {
                return data.map(function (item) { return new Car(item) });
            }
            else {
                return new Car(item);
            }
        });
    };

    function refreshObj(obj) {
       return svc.get(function (item) { return item.Id = obj.Id }).then(function (response) {
            var car = response[0];
            angular.extend(obj, car);
            return obj;
        });
    };

    function edit(id, newObj) {
        return storageSvc.edit(entityName, id, new Car(newObj)).then(function (data) {
            return new Car(data);
        });
    };

    function push(newObj) {
        return storageSvc.push(entityName, new Car(newObj)).then(function (data) {
            return new Car(data);
        });
    };

}]);
myApp.service('categorySvc', ['storageSvc', 'Entity', 'Category', function (storageSvc, Entity, Category) {

    var entitiyName = Entity.Category;
    
    var svc = angular.extend(this, {
        get: get
    });

    function get(filter) {
        return storageSvc.getEntitiesByName(entitiyName, filter).then(function (data) {
            if (angular.isArray(data)) {
                return data.map(function (item) { return new Category(item) });
            }
            else {
                return new Category(item);
            }
        });
    }

}]);
myApp.service('commentSvc', ['storageSvc', 'Entity', 'Comment', function (storageSvc, Entity, Comment) {

    var entityName = Entity.Comment;
    
    var svc = angular.extend(this, {
        get: get,
        push: push,
        edit: edit,
    });

    function get(filter) {
        return storageSvc.getEntitiesByName(entityName, filter).then(function (data) {
            if (angular.isArray(data)) {
                return data.map(function (item) { return new Comment(item) });
            }
            else {
                return new Comment(item);
            }
        });
    };

    function edit(id, newObj) {
        return storageSvc.edit(entityName, id, new Comment(newObj)).then(function (data) {
            return new Comment(data);
        });
    };

    function push(newObj) {
        return storageSvc.push(entityName, new Comment(newObj)).then(function (data) {
            return new Comment(data);
        });
    };

}]);
myApp.service('orderSvc', ['storageSvc', 'Entity', 'Order', 'carSvc', function (storageSvc, Entity, Order, carSvc) {

    var entityName = Entity.Order;

    var svc = angular.extend(this, {
        get: get,
        push: push,
        order: order,
        edit: edit
    });

    function get(filter) {
        return storageSvc.getEntitiesByName(entityName, filter).then(function (data) {
            if (angular.isArray(data)) {
                return data.map(function (item) { return new Order(item) });
            }
            else {
                return new Order(item);
            }
        });
    };

    function push(newObj) {
        return storageSvc.push(entityName, new Order(newObj)).then(function (data) {
            return new Order(data);
        });
    };

    function edit(id, newObj) {
        return storageSvc.edit(entityName, id, newObj).then(function (data) {
            return new Order(data);
        });
    };

    function order(newObj) {
        return push(newObj).then(function (order) {
            return order.joinCar().then(function (joinedOrder) {
                var car = joinedOrder.Car;
                car.Available = car.Available > 1 ? car.Available - 1 : 0;
               return carSvc.edit(car.Id, car).then(function (updatedCar) {
                    if (updatedCar) {
                        order.Car = updatedCar;
                        return order;
                    }
                });
            });
        });
    };

}]);
myApp.service('rateSvc', ['storageSvc', 'Entity', 'Rate', function (storageSvc, Entity, Rate) {
    var entityName = Entity.Rate;

    var svc = angular.extend(this, {
        get: get,
        push: push,
        edit: edit,
        rate: rate
    });

    function get(filter) {
        return storageSvc.getEntitiesByName(entityName, filter).then(function (data) {
            if (angular.isArray(data)) {
                return data.map(function (item) { return new Rate(item) });
            }
            else {
                return new Rate(item);
            }
        });
    };

    function rate(userId, carId, rate) {
        return svc.get(function (item) { return item.CarId == carId && item.UserId == userId }).then(function (results) {
            if (results && results[0]) { // then update
                var edited = results[0];
                edited.Rate = rate;
                edited.Date = new Date();

                return svc.edit(edited.Id, edited).then(function (editedRate) {
                    return editedRate;
                });
            }
            else {
                return svc.push({
                    Rate: rate,
                    UserId: userId,
                    CarId: carId,
                    Date: new Date()
                }).then(function (addedRate) {
                    return addedRate;
                });
            }
        });
    };

    function edit(id, newObj) {
        return storageSvc.edit(entityName, id, new Rate(newObj)).then(function (data) {
            return new Rate(data);
        });
    };

    function push(newObj) {
        return storageSvc.push(entityName, new Rate(newObj)).then(function (data) {
            return new Rate(data);
        });
    };

}]);
myApp.service('siteSvc', ['$mdDialog', function ($mdDialog) {

    
    var svc = angular.extend(this, {
        openAlert : openAlert
    });

    function openAlert(title, textContent, ok, callback) {
        $mdDialog.show(
              $mdDialog.alert()
                .parent(angular.element(document.querySelector('body')))
                .clickOutsideToClose(true)
                .title(title)
                .textContent(textContent)
                .ariaLabel('Informacja')
                .ok(ok)
            ).then(function () {
                if (callback) {
                    callback();
                }
            });
    };

}]);
myApp.service('storageSvc', ['$http', '$rootScope', 'AppKey', '$templateCache', function ($http, $rootScope, AppKey, $templateCache) {

    var svc = angular.extend(this, {
        getEntitiesByName: getEntitiesByName,
        edit: edit,
        push: push
    });

    var entityPathFormatter = function (EntityName) {
        return "app/storage/" + EntityName + ".json";
    };

    function getEntitiesByName(entityName, filterFunction) {
        $templateCache.remove(entityPathFormatter(entityName));
        return $http.get(entityPathFormatter(entityName), { cache: false }).then(function (response) {
            if (filterFunction && angular.isArray(response.data)) {
                return response.data.filter(filterFunction);
            }
            else {
                return response.data;
            }
        });
    };

    function edit(entityName, id, newObj) {
        $templateCache.remove('app/Api/EditEntity');

        return $http.post('app/Api/EditEntity', {
            entityName: entityName,
            id: id,
            newObj: newObj
        }).then(function (response) {
            $rootScope.$broadcast(AppKey.Edit + entityName,
            {
                newObj: response.data,
                id: id
            });
            return response.data;
        });
    };


    function push(entityName, newObj) {
        $templateCache.remove('app/Api/PushEntity');

        return $http.post('app/Api/PushEntity', {
            entityName: entityName,
            newObj: newObj
        }).then(function (response) {
            return response.data;
        });
    };

}]);
myApp.service('userSvc', ['storageSvc', 'Entity', 'User', '$cookies', 'AppKey', '$crypto', '$q', '$location', function (storageSvc, Entity, User, $cookies, AppKey, $crypto, $q, $location) {

    var entityName = Entity.User;

    var svc = angular.extend(this, {
        get: get,
        push: push,
        login: login,
        isLogin: false,
        isAdmin: false,
        logout: logout,
        checkIsLogin: checkIsLogin,
        getUserId: getUserId,
        getLoggedUser: getLoggedUser,
        putCookie: putCookie
    });

    function getUserId() {
        return parseInt($cookies.get(AppKey.CookieLogin));
    };

    function getLoggedUser() {
        return svc.get(function (item) { return item.Id == svc.getUserId() }).then(function (users) {
            if (users && users[0]) {
                return users[0];
            }
            else {
                return null;
            }
        });
    };

    function get(filter) {
        return storageSvc.getEntitiesByName(entityName, filter).then(function (data) {
            if (angular.isArray(data)) {
                return data.map(function (item) { return new User(item) });
            }
            else {
                return new User(item);
            }
        });
    };

    function push(newObj) {
        return storageSvc.push(entityName, new User(newObj)).then(function (data) {
            return new User(data);
        });
    };

    function login(Email, Password) {
        return svc.get(function (item) { return item.Email == Email; }).then(function (users) {
            var user = users[0];

            var encrypted = $crypto.decrypt(user.Password);

            if (encrypted == Password) {
                $cookies.put(AppKey.CookieLogin, user.Id);
                svc.isLogin = true;
                svc.isAdmin = user.IsAdmin;
                $location.path('/');
                return true;
            }
            else {
                return false;
            }
        });
    };
    function putCookie(userId) {
        $cookies.put(AppKey.CookieLogin, userId);
    };

    function logout() {
        

        svc.get(function (item) { return item.Id = svc.getUserId() }).then(function (users) {
            if (users &&  users[0] != null && users[0].FacebookId != null) {
                Fb.logout();
            }
            svc.isLogin = false;
            svc.isAdmin = false;
            $cookies.remove(AppKey.CookieLogin);
            $location.path('/');
        });
    };

    function checkIsLogin() {
        var cookie = svc.getUserId();
        svc.get(function (item) { return item.Id == cookie }).then(function (response) {
            if (cookie && response && response[0]) {
                svc.isLogin = true;
                svc.isAdmin = response[0].IsAdmin;
            }
        });
    };
}]);


myApp.factory('Car', [function () {

    var model = {
        Id: null,
        Name: null,
        Amount: null,
        Description: null,
        Location: null,
        AvatarPath: null,
        Count: null,
        Available: null,
        IsDeleted: false,

        UserId: null, // owner
        CategoryIds: []
    };

    var car = function (obj) {
        angular.extend(this, model, obj);
    };

    return car;
}]);
myApp.factory('Category', ['carSvc', function (carSvc) {
    var model = {
        Id: null,
        Name: null,
        Cars: [] //Category.id => Car.CategoryIds
    };

    var category = function (obj) {
        angular.extend(this, model, obj);
    };

    category.prototype.joinCars = function () {
        var data = this;

        return carSvc.get(function (car) {
            return car.CategoryIds.indexOf(data.Id) > -1
        }).then(function (cars) {
            data.Cars = cars;
            return data;
        });
    }

    return category;
}]);
myApp.factory('Comment', ['userSvc', function (userSvc) {

    var model = {
        Id: null,
        Comment: null,
        Date: null,
        UserId: null,
        CarId: null,
        IsDeleted: false
        //User: null // Commen.UserId => User.Id
    };

    var comment = function (obj) {
        angular.extend(this, model, obj);
    };

    comment.prototype.joinUser = function () {
        var data = this;
        return userSvc.get(function (user) {
            return user.Id == data.UserId;
        }).then(function (users) {
            data.User = users[0];
            return data;
        });
    };

    return comment;
}]);
myApp.factory('Order', ['carSvc', 'userSvc', function (carSvc, userSvc) {

    var model = {
        Id: null,
        UserId: null,
        CarId: null,
        DateFrom: null,
        DateTo: null,
        DaysCount: null,
        Amount: null,
        IsReturned: null,
        IsPaid: null

        //User: null, // Order.UserId => User.Id
        //Car: null // Order.CarId => Car.Id
    };

    var order = function (obj) {
        var data = angular.copy(angular.extend(model, obj));

        angular.extend(this, data);
    };

    order.prototype.joinCar = function () {
        var data = this;
        return carSvc.get(function (car) {
            return car.Id == data.CarId;
        }).then(function (cars) {
            data.Car = cars[0];
            return data;
        });
    };

    order.prototype.joinUser = function () {
        var data = this;
        return userSvc.get(function (car) {
            return car.Id == data.UserId;
        }).then(function (users) {
            data.User = users[0];
            return data;
        });
    };

    return order;
}]);
myApp.factory('Rate', [function () {

    var model = {
        Id: null,
        UserId: null,
        CarId: null,
        Rate: null,
        Date: null

        //User: null, // Rate.UserId => User.Id
        //Car: null // Rate.CarId => Car.Id
    };

    var rate = function (obj) {
        angular.extend(this, model, obj);
    };

    rate.prototype.joinCar = function () {
        var data = this;
        return carSvc.get(function (car) {
            return car.Id == data.CarId;
        }).then(function (cars) {
            data.Car = cars[0];
            return data;
        });
    };

    rate.prototype.joinUser = function () {
        var data = this;
        return userSvc.get(function (car) {
            return car.Id == data.UserId;
        }).then(function (users) {
            data.User = users[0];
            return data;
        });
    };

    return rate;
}]);
myApp.factory('User', [function () {

    var model = {
        Id: null,
        Name: null,
        Password: null,
        IsAdmin: false,
        Email: null,
        FacebookId: null,
        Age: 18
    };

    var user = function (obj) {
       angular.extend(this, model, obj);

    };

    return user;
}]);
myApp.constant('AppKey', {
    CookieLogin: 'Login',
    Newsletter: 'Newsletter',
    CryptoKey: "MySuperSecretKey!@#",
    Edit: 'Edit'
});
if (isDemoVersion) {
    myApp.constant('Entity', {
        Category: 'Demo_Category',
        Car: 'Demo_Car',
        Order: 'Demo_Order',
        User: 'Demo_User',
        Rate: 'Demo_Rate',
        Comment: 'Demo_Comment'
    });
}
else {
    myApp.constant('Entity', {
        Category: 'Category',
        Car: 'Car',
        Order: 'Order',
        User: 'User',
        Rate: 'Rate',
        Comment: 'Comment'
    });
}
