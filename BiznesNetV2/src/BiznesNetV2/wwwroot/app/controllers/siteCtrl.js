﻿myApp.controller('siteCtrl', ['userSvc', '$mdDialog', 'carSvc', '$cookies', 'AppKey', '$scope', '$location', '$rootScope','$timeout',
    function (userSvc, $mdDialog, carSvc, $cookies, AppKey, $scope, $location, $rootScope, $timeout) {
        var site = angular.extend(this, {
            init: init,
            userSvc: userSvc,
            loginOpen: loginOpen,
            registerOpen: registerOpen,
            logout: logout,
            carAddOpen: carAddOpen,
            top3Cars: [],
            getNewsletterCookie: getNewsletterCookie,
            saveNewsletter: saveNewsletter,
            textSearch: "",
            userName: ""
        });
        function getNewsletterCookie() {
            return $cookies.get(AppKey.Newsletter) || false;
        };
        $scope.userSvc = userSvc;

        $scope.$watch('userSvc.isLogin', function (newVal, oldVal) {
            userSvc.getLoggedUser().then(function (user) {
                site.userName = user ? user.Name : '';
            });
        });

        function saveNewsletter() {
            site.newsletterForm.$setSubmitted();
            if (!site.newsletterForm.$valid) return ctr.hireForm.$valid;
            $cookies.put(AppKey.Newsletter, true);
        };

        function DialogController($scope, $mdDialog) {
            $scope.hide = function () {
                $mdDialog.hide();
            };

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        }

        function loginOpen(ev) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/templates/Login.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: false
            })
        };

        function registerOpen(ev) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/templates/Register.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: false
            })
        };

        function carAddOpen(ev) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/templates/AddCar.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: false
            });
        };


        function logout() {
            userSvc.logout();
        };

        function setupTop3Cars() {
            return carSvc.get().then(function (data) {
                site.top3Cars = data.filter(function(car){return car.IsDeleted != true}).slice(0, 3);
            });
        };

        var history = [];

        $rootScope.$on('$routeChangeSuccess', function () {
            history.push($location.$$path);
        });

        $rootScope.back = function () {
            var prevUrl = history.length > 1 ? history.splice(-2)[0] : "/";
            $location.path(prevUrl);
        };

        function fbInit() {
            $timeout(function () {
                FB.getLoginStatus(function (response) {
                    if (response.status == 'connected') {
                        var userFbId = response.authResponse.userID;

                        userSvc.get(function (item) { return item.FacebookId == userFbId; }).then(function (users) {
                            if (users && users[0]) {
                                userSvc.putCookie(users[0].Id);
                                userSvc.isLogin = true;
                            }
                        });
                    }
                });
            }, 400);
        };

        function init() {
            userSvc.checkIsLogin();
            setupTop3Cars();
            fbInit();
        };

    }]);