﻿myApp.controller('orderListCtrl', ['orderSvc', 'siteSvc', 'userSvc', '$q', '$mdDialog', 'carSvc', function (orderSvc, siteSvc, userSvc, $q, $mdDialog, carSvc) {


    var ctr = angular.extend(this, {
        init: init,
        orders: [],
        getFilteredOrders: getFilteredOrders,
        tryToDate: tryToDate,
        payOpen: payOpen
    });

    function DialogController($scope, $mdDialog, order) {
        $scope.order = order;

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.answer = function (answer) {
            $mdDialog.hide(answer);
        };

        $scope.pay = function (order) {
            if (userSvc.isAdmin == true) {
                return orderSvc.edit(order.Id, { IsPaid: true }).then(function (editedOrder) {
                    order.IsPaid = editedOrder.IsPaid;

                    return carSvc.get(function (item) { return item.Id == editedOrder.CarId })
                                  .then(function (cars) {
                                      var car = cars[0];
                                      car.Available = car.Available + 1;
                                      return carSvc.edit(car.Id, car).then(function (editedCar) {
                                          $mdDialog.cancel();
                                      });
                                  });
                });
            }
            else {
                siteSvc.openAlert("Płatność", "Dla bezpieczeństwa funkcjonalność jest zablokowama, system działa na potrzeby projektu zaliczeniowego.", "zamknij");
            }
        };
    }

    function payOpen(ev, order) {
        if (userSvc.isLogin == false)
            siteSvc.openAlert('Logowanie', 'Prosze się zalogować lub zarejestrować', 'Zamknij');
        else
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/templates/PayDialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: false,
                locals: { order: order }
            });
    };

    function tryToDate(order) {
        return (new Date(order.DateTo)) <= new Date() && order.IsPaid == false ? true : false;
    };

    function getFilteredOrders(search) {
        if (search) {

            return ctr.orders.filter(function (order) { return order.Car.Name.toLowerCase().indexOf(search.toLowerCase()) !== -1 || order.Car.Description.toLowerCase().indexOf(search.toLowerCase()) !== -1 || order.Car.Location.toLowerCase().indexOf(search.toLowerCase()) !== -1 });
        }
        return ctr.orders;
    };

    function setupOrdersList() {
        return orderSvc.get(function (item) { return userSvc.isAdmin == false ? item.UserId == userSvc.getUserId() : true }).then(function (orders) {
            var joinPromises = [];
            if (orders) {
                orders.forEach(function (order) { joinPromises.push(order.joinCar()) });

                $q.all(joinPromises).then(function (joinedOrders) {
                    ctr.orders = joinedOrders;
                });
            }
        });
    };

    function init() {
        setupOrdersList()
    };

}]);