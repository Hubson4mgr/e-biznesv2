﻿myApp.controller('hireCarCtrl', ['orderSvc', '$cookies', 'AppKey', '$mdDialog','carSvc','siteSvc','$rootScope',
    function (orderSvc, $cookies, AppKey, $mdDialog, carSvc, siteSvc, $rootScope) {

        var hireModel = {
            DateFrom: null,
            DateTo: null,
            CarId: null,
            UserId: null,
            DaysCount: null,
            Amount: null,
            IsReturned: false
        };

        var ctr = angular.extend(this, {
            init: init,
            hireModel: hireModel,
            hireSubmit: hireSubmit,
            calculatedAmount: calculatedAmount,
            car: null,
        });

        function calculateDays(dateFrom, dateTo) {
            var oneDay = 24 * 60 * 60 * 1000;
            return Math.round(Math.abs((dateFrom.getTime() - dateTo.getTime()) / (oneDay)));
        };

        function calculatedAmount() {
            if (!(ctr.hireModel.DateFrom && ctr.hireModel.DateTo)) return null;

            var days = calculateDays(ctr.hireModel.DateFrom, ctr.hireModel.DateTo);
            var dayAmount = ctr.car.Amount;
            ctr.hireModel.DaysCount = days;
            ctr.hireModel.Amount = days * dayAmount;

            return ctr.hireModel.Amount;
        }

        function hireSubmit() {
            ctr.hireForm.$setSubmitted();
            if (!ctr.hireForm.$valid) return ctr.hireForm.$valid;
            if (ctr.car.Available == 0) {
                siteSvc.showAlert('Brak samochodów', 'Prosimy spróbować w późniejszym terminie', 'Zamknij');
                return;
            } 

            ctr.hireModel.Amount = calculatedAmount(ctr.hireModel.DaysCount, ctr.car.Amount);

            orderSvc.order({
                UserId: ctr.hireModel.UserId,
                CarId: ctr.hireModel.CarId,
                DateFrom: ctr.hireModel.DateFrom,
                DateTo: ctr.hireModel.DateTo,
                DaysCount: ctr.hireModel.DaysCount,
                Amount: calculatedAmount(),
                IsReturned: ctr.hireModel.IsReturned,
                IsPaid: false
            }).then(function (data) {
                if (data) {
                    carSvc.refreshObj(data.Car).then(function () {
                        angular.extend(ctr.car, data.Car);
                    });
                   

                    $mdDialog.cancel();
                }
            });
        };

        function init(car) {
            ctr.car = car;
            ctr.hireModel.CarId = car.Id;
            ctr.hireModel.UserId = parseInt($cookies.get(AppKey.CookieLogin));
        };

    }]);