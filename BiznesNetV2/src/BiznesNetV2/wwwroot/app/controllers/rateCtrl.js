﻿myApp.controller('rateCtrl', ['userSvc', '$cookies', 'AppKey', 'rateSvc', '$scope',
    function (userSvc, $cookies, AppKey, rateSvc, $scope) {
        $scope.model = null;
        $scope.userSvc = userSvc;

        var ctr = angular.extend(this, {
            init: init,
            isReadOnlyRate: isReadOnlyRate,
            car: null
        });

        function isReadOnlyRate() {
            return $cookies.get(AppKey.CookieLogin) == null;
        };

        function setupWatch() {
            $scope.$watch('model', function (newVal, oldVal) {
                var userId = parseInt($cookies.get(AppKey.CookieLogin));
                if (newVal != oldVal && userId && oldVal != null) {
                    rateSvc.rate(userId, ctr.car.Id, newVal);
                }
            });
        }
      
        function setupRate() {
            var userId = parseInt($cookies.get(AppKey.CookieLogin));
            if (userId) {
                rateSvc.get(function (item) { return item.CarId == ctr.car.Id && item.UserId == userId }).then(function (results) {
                    if (results && results[0]) {
                        $scope.model = results[0].Rate;
                    } else {
                        $scope.model = 0;
                    }
                });
            }
            else {
                rateSvc.get(function (item) { return item.CarId == ctr.car.Id }).then(function (results) {
                    if (results) {
                        var sum = 0;
                        for (var item in results) {
                            sum = sum + results[item].Rate;
                        }
                        $scope.model = sum / results.length;
                    }
                    else {
                        $scope.model = 0;
                    }
                });
            }
        }

        function init(car) {
            ctr.car = car;
            setupRate();
            setupWatch();
        };

        $scope.$watch('userSvc.isLogin', function (newVal, oldVal) {
            setupRate();
        });

    }]);