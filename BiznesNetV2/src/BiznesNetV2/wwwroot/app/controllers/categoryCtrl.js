﻿myApp.controller('categoryCtrl', ['categorySvc', '$routeParams', function (categorySvc, $routeParams) {

    var ctr = angular.extend(this, {
        init: init,
        categories: categories,
        isActive: isActive
    });

    var categories = [];

    function loadCategories() {
        categorySvc.get().then(function (data) {
            ctr.categories = data;
        });
    };

    function isActive(categoryName) {
        return $routeParams.categoryName == categoryName;
    };

    function init() {
        loadCategories();
    };
    
}]);