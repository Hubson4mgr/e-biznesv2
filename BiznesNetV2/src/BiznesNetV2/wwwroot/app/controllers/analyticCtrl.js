﻿myApp.controller('analyticCtrl', ['carSvc', '$window', 'orderSvc', 'userSvc', 'commentSvc', '$timeout', 'categorySvc', '$q', function (carSvc, $window, orderSvc, userSvc, commentSvc, $timeout, categorySvc, $q) {

    var ctr = angular.extend(this, {
        init: init,
        ordersPerMonth: {},
        topCategories: {},
        topDays: {}
    });



    function setupOrdersPerMonths() {
        orderSvc.get().then(function (orders) {
            var grouped = _.groupBy(orders, function (order) { return new Date(order.DateFrom).getMonth() + 1; });

            var data = [];

            for (var i = 1; i <= 12; i++) {
                if (grouped[i]) {
                    data.push(grouped[i].length);
                }
                else {
                    data.push(0);
                }
            }


            var model = {
                labels: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
                series: ['Liczba zamówień'],
                data: [
                        data
                ],
                onClick: angular.noop,
            };

            ctr.ordersPerMonth = model;
            ctr.ordersPerMonth.isInited = true;
        });
    };

    function setupTopCategories() {
        categorySvc.get().then(function (categories) {
            orderSvc.get().then(function (orders) {
                $q.all(orders.map(function (item) { return item.joinCar() })).then(function () {
                    var orderedAllCars = orders.map(function (order) { return order.Car });
                    var merged = [].concat.apply([], orderedAllCars.map(function (car) { return car.CategoryIds }));
                    var grouped = _.groupBy(merged, function (categoryId) { return categoryId });
                    var data = [];

                    categories.forEach(function (category) {
                        if (grouped[category.Id]) {
                            data.push(grouped[category.Id].length);
                        }
                        else {
                            data.push(0);
                        }
                    });

                    var model = {
                        labels: categories.map(function (item) { return item.Name }),
                        series: ['Liczba zamówień'],
                        data: [
                                data
                        ],
                        onClick: angular.noop,
                    };
                    ctr.topCategories = model;
                    ctr.topCategories.isInited = true;
                });
            });
        });
    };

    function setupTopDays() {

        orderSvc.get().then(function (orders) {
            var days = orders.map(function (item) {
                return item.DaysCount;
            })
            .sort(function (a, b) {
                return a - b;
            });

            var grouped = _.groupBy(days, function (item) { return item });
            var data = [];
            var labels = [];
            for (var item in grouped) {
                data.push(grouped[item].length)
                labels.push(item + " dni");
            }
            var model = {
                labels: labels,
                data: data,
                onClick: angular.noop,
            };
            ctr.topDays = model;
            ctr.topDays.isInited = true;
        });

    };

    function init() {
        $timeout(function () {
            setupOrdersPerMonths();
            setupTopCategories();
            setupTopDays();
        }, 100);

    };

}]);