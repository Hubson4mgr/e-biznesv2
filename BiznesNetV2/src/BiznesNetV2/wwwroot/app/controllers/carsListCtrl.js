﻿myApp.controller('carsListCtrl', ['$routeParams', 'categorySvc', '$scope', 'carSvc', 'AppKey', function ($routeParams, categorySvc, $scope, carSvc, AppKey) {
    var categoryName = $routeParams.categoryName;

    var ctr = angular.extend(this, {
        init: init,
        category: categoryName,
        cars: [],
        getCars: getCars
    });

    function getCars(search) {
        if (search) {
            return ctr.cars.filter(function (car) { return car.Name.toLowerCase().indexOf(search.toLowerCase()) !== -1 || car.Description.toLowerCase().indexOf(search.toLowerCase()) !== -1 || car.Location.toLowerCase().indexOf(search.toLowerCase()) !== -1 });
        }
        return ctr.cars;
    }

    function loadCarsByCategoryName(categoryName) {
        return categorySvc.get(function (item) { return item.Name == categoryName }).then(function (data) {
            var categoryObj = data[0];
            return categoryObj.joinCars().then(function (category) {
                return category.Cars.filter(function(car){return car.IsDeleted!= true});
            });

        });
    };

    $scope.$on('reload-car-list', init);
    $scope.$on(AppKey.Edit + carSvc.entityName, function (eventParam) {
        init();
    });

    function init() {
        if (categoryName) {
            loadCarsByCategoryName(categoryName).then(function (cars) { ctr.cars = cars });
        }
    };

}]);