﻿myApp.controller('facebookCtrl', ['$scope', '$cookies', 'userSvc', '$mdDialog', '$location',
function ($scope, $cookies, userSvc, $mdDialog, $location) {

    var ctr = angular.extend(this, {
        login: login,

    });


    function registerSubmit(name, email, facebookId) {
        return userSvc.push({
            Name: name,
            Email: email,
            FacebookId: facebookId,
            Age: 18
        })
    };


    function login() {
        FB.login(function (response) {
            FB.api('/me', 'GET', { fields: 'id, name, first_name, email' }, function (response) {
                if (response.id) {
                    userSvc.get(function (item) { return item.FacebookId == response.id }).then(function (users) {
                        if (!(users && users[0])) {
                            registerSubmit(response.name, response.email, response.id).then(function (addedUser) {
                                userSvc.putCookie(addedUser.Id);
                                userSvc.isLogin = true;
                                $mdDialog.cancel();
                                $location.path('/');
                            });
                        }
                        else {
                            userSvc.putCookie(users[0].Id);
                            userSvc.isLogin = true;
                            $mdDialog.cancel();
                            $location.path('/');
                        }
                    });
                }
            });
        }, { scope: 'email' });


        //FB.login(function (response) {
        //    debugger;
        //});
    }

}]);