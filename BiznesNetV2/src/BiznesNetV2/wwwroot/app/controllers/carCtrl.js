﻿myApp.controller('carCtrl', ['carSvc', '$mdDialog', '$scope', 'Upload', 'categorySvc', '$cookies', 'AppKey', '$timeout','$rootScope',
    function (carSvc, $mdDialog, $scope, Upload, categorySvc, $cookies, AppKey, $timeout,$rootScope) {

    var carModel = {
        Name: null,
        Description: null,
        Amount: 0.0,
        Location: null,
        AvatarPath: null,
        UserId: parseInt($cookies.get(AppKey.CookieLogin)),
        Count: 1,
        Available: 1,
        CategoryIds: [],
    };


    var ctr = angular.extend(this, {
        init: init,
        carModel: carModel,
        carAddSubmit: carAddSubmit,
        upload: upload,
        selectedCategories: null,
        categories: []
    });



    function carAddSubmit() {
        carModel.CategoryIds = ctr.selectedCategories.map(function (item) { return JSON.parse(item).Id });
        carSvc.push(carModel).then(function (car) {
            if (car) {
                $mdDialog.cancel();
                $rootScope.$broadcast('reload-car-list', true);
            }
        });
    };

    function upload(file) {
        if (file) {
            file.upload = Upload.upload({
                url: '/app/Api/UploadFile',
                data: { file: file }
            }).then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                    carModel.AvatarPath = response.data.filePath;
                });
            });
        }

    };
    
    function setupCategories() {
        categorySvc.get().then(function (categories) {
            ctr.categories = categories;
        });
    };


    function init() {
        setupCategories();
    };
    
}]);