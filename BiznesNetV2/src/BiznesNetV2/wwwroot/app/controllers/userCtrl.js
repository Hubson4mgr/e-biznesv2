﻿myApp.controller('userCtrl', ['userSvc', '$crypto', '$mdDialog', function (userSvc, $crypto, $mdDialog) {

    var registerModel = {
        Name : null,
        Email : null,
        Password: null,
        Age: 18
    };

    var loginModel = {
        Email: null,
        Password: null
    };

    var ctr = angular.extend(this, {
        init: init,
        registerModel: registerModel,
        loginModel: loginModel,
        registerSubmit: registerSubmit,
        loginSubmit: loginSubmit
    });

    function registerSubmit() {
        ctr.registerForm.$setSubmitted();
        if (!ctr.registerForm.$valid) return ctr.registerForm.$valid

        userSvc.push({
            Name: ctr.registerModel.Name,
            Email: ctr.registerModel.Email,
            Password: $crypto.encrypt(ctr.registerModel.Password),
            Age: ctr.registerModel.Age
        }).then(function (user) {
            userSvc.login(user.Email, ctr.registerModel.Password).then(function (isSuccess) {
                if (isSuccess == true) {
                    $mdDialog.cancel();
                }
            });
        });
    };

    function loginSubmit() {
        ctr.loginForm.$setSubmitted();
        if (!ctr.loginForm.$valid) return ctr.loginForm.$valid

        userSvc.login(ctr.loginModel.Email, ctr.loginModel.Password).then(function (isSuccess) {
            if (isSuccess == true) {
                $mdDialog.cancel();
            }
        });
    };

    function init() {
        loadCategories();
    };
    
}]);