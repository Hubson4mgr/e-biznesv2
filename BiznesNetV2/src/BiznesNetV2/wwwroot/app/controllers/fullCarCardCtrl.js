﻿myApp.controller('fullCarCardCtrl', ['carSvc', '$window', '$scope', '$routeParams', '$rootScope', 'userSvc', 'commentSvc', '$q', '$timeout', 'siteSvc','$location',
function (carSvc, $window, $scope, $routeParams, $rootScope, userSvc, commentSvc, $q, $timeout, siteSvc, $location) {
    var carId = $routeParams.carId;

    var ctr = angular.extend(this, {
        init: init,
        goBack: goBack,
        car: null,
        isCarLoaded: false,
        comments: [],
        getComments: getComments,
        addComment: addComment,
        removeCar: removeCar,
        removeComment: removeComment,
        isUserComment: isUserComment
    });
    var nowIsAddingComment = false;
    function goBack() {
        $rootScope.back();
    };

    function setupCar() {
        return carSvc.get(function (item) { return item.Id == carId }).then(function (cars) {
            var car = cars[0];
            if (car) {
                ctr.car = car;
            }
            ctr.isCarLoaded = true;

            return car;
        });
    };

    function setupComments() {
        return commentSvc.get(function (item) { return item.CarId == ctr.car.Id })
                    .then(function (comments) {
                        ctr.comments = comments;
                        if (ctr.comments) {
                            var joinPromises = ctr.comments.map(function (item) { return item.joinUser() });

                            return $q.all(joinPromises).then(function () {
                                return comments.filter(function (com) { return com.IsDeleted != true});
                            });
                        }
                        else {
                            return [];
                        }
                    });
    };

    function getComments(filter) {
        return nowIsAddingComment == false ? ctr.comments.filter(function (item) {
            return (item.Comment.toLowerCase().indexOf(filter.toLowerCase()) !== -1 ||
                   item.User.Name.toLowerCase().indexOf(filter.toLowerCase()) !== -1) && item.IsDeleted != true
        }) : []
    };

    function addComment() {
        ctr.commentForm.$setSubmitted();
        if (!ctr.commentForm.$valid) return ctr.commentForm.$valid;
        commentSvc.push({
            CarId: ctr.car.Id,
            UserId: userSvc.getUserId(),
            Comment: ctr.comment,
            Date: new Date()
        }).then(function () {
            nowIsAddingComment = true;
            setupComments().then(function () {
                ctr.comment = "";
                nowIsAddingComment = false;
            });
        })
    };

    function removeCar() {
        ctr.car.IsDeleted = true;
        carSvc.edit(ctr.car.Id, ctr.car).then(function () {
            
            siteSvc.openAlert('', 'Auto zostało usunięte', 'Ok', function () { $location.path('/'); });
        });
    };

    function removeComment(comment) {
        comment.IsDeleted = true;
        commentSvc.edit(comment.Id, comment).then(function () {
            siteSvc.openAlert('', 'Komentarz został usunięty', 'Ok', function () {
                setupComments();
            });
        });
    };

    function isUserComment(comment) {
        return userSvc.getUserId() == comment.UserId;
    };

    function init() {
        $window.scrollTo(0, 0);
        setupCar().then(setupComments);
    };

}]);