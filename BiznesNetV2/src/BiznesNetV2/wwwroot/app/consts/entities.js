﻿if (isDemoVersion) {
    myApp.constant('Entity', {
        Category: 'Demo_Category',
        Car: 'Demo_Car',
        Order: 'Demo_Order',
        User: 'Demo_User',
        Rate: 'Demo_Rate',
        Comment: 'Demo_Comment'
    });
}
else {
    myApp.constant('Entity', {
        Category: 'Category',
        Car: 'Car',
        Order: 'Order',
        User: 'User',
        Rate: 'Rate',
        Comment: 'Comment'
    });
}
