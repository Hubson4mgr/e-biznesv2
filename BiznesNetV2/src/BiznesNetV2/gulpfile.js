﻿/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/


var gulp = require("gulp"),
    rimraf = require("rimraf"),
    plugins = require('gulp-load-plugins')();


var paths = {
    webroot: "./wwwroot/"
};

paths.angularApp = {
    appJs: paths.webroot + 'app/app.js',
    controllers: paths.webroot + 'app/**/controllers/**/*.js',
    directives: paths.webroot + 'app/**/directives/**/*.js',
    services: paths.webroot + 'app/**/services/**/*.js',
    factories: paths.webroot + 'app/**/factories/**/*.js',
    consts: paths.webroot + 'app/**/consts/**/*.js',
    destName: 'site.js',
    dest: './wwwroot/app'
};


gulp.task('angularApp', function () {
    var angularApp = [paths.angularApp.appJs,
        paths.angularApp.controllers,
        paths.angularApp.directives,
        paths.angularApp.services,
        paths.angularApp.factories,
        paths.angularApp.consts
    ];

    return gulp.src(angularApp)
        .pipe(plugins.concat(paths.angularApp.destName))
        .pipe(gulp.dest(paths.angularApp.dest));
});

gulp.task('watch:js', ['angularApp'], function () {
    gulp.watch(paths.webroot + 'app/**/*.js', ['angularApp']);
});

gulp.task('build', ['angularApp']);