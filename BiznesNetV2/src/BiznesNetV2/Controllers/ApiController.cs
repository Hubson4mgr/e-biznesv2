﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using BiznesNetV2.Model;
using System.IO;
using Newtonsoft.Json.Linq;
using Microsoft.Net.Http.Headers;
using BiznesNetV2.Helper;

namespace BiznesNetV2.Controllers
{
    [AllowAnonymous]
    public class ApiController : Controller
    {
        [HttpPost]
        public IActionResult EditEntity([FromBody] Edit model)
        {
            dynamic result = Storage.Edit(model); ;
            return Json(result);
        }

        [HttpPost]
        public IActionResult PushEntity([FromBody] Push model)
        {
            dynamic result = Storage.Push(model); ;
            return Json(result);
        }

        [HttpPost]
        public IActionResult UploadFile()
        {
            var _file = Request.Form.Files[0];

            var app = Directory.GetCurrentDirectory();

            var fileName = Guid.NewGuid();
            var filePath = $"{app}/wwwroot/images/cars/{fileName}.jpg";
            string fileByte;

            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                _file.CopyTo(fileStream);
                fileByte = fileStream.ToString();
            }

            return Json(new
            {
                fileName = fileName,
                filePath = $"/images/cars/{fileName}.jpg",
                bytes = fileByte
            });
        }
    }
}
