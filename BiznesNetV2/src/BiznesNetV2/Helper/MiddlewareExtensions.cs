﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BiznesNetV2.Helper
{

    public class SyncStorageMiddleware
    {
        private readonly RequestDelegate _next;
        public SyncStorageMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        private  void CopyDirectory(DirectoryInfo source, DirectoryInfo destination)
        {
            if (!destination.Exists)
            {
                destination.Create();
            }

            FileInfo[] files = source.GetFiles();
            foreach (FileInfo file in files)
            {
                file.CopyTo(Path.Combine(destination.FullName,
                    file.Name));
            }

            DirectoryInfo[] dirs = source.GetDirectories();
            foreach (DirectoryInfo dir in dirs)
            {
                string destinationDir = Path.Combine(destination.FullName, dir.Name);
                CopyDirectory(dir, new DirectoryInfo(destinationDir));
            }
        }



        public async Task Invoke(HttpContext httpContext)
        {
            var now = DateTime.Now;
            var app = Directory.GetCurrentDirectory();
            var configFile = $"{app}/sync-storage.json";

            var json = System.IO.File.ReadAllText(configFile);
            var objects = JsonConvert.DeserializeObject<dynamic>(json);
            var lastSync = objects["LastSyncDate"];
            DateTime dateLastSync = DateTime.Parse(lastSync.Value);
            if (lastSync != null && now.Year == dateLastSync.Year && now.Month == dateLastSync.Month && now.Day > dateLastSync.Day)
            {

                CopyDirectory(new DirectoryInfo($"{app}/wwwroot/app/storage"), new DirectoryInfo($"{app}/sync-data/{now.ToShortDateString()}"));

                var serialized = JsonConvert.SerializeObject(new { LastSyncDate  = now.ToShortDateString()}, Formatting.Indented);
                System.IO.File.WriteAllText(configFile, serialized);
            }

            await _next.Invoke(httpContext);
        }
    }

    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder SyncStorage(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<SyncStorageMiddleware>();
        }
    }
}
