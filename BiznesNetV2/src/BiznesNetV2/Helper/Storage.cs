﻿using BiznesNetV2.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BiznesNetV2.Helper
{
    public static class Storage
    {
        private static bool _isBusyNow = false;
        public static dynamic Push(Push model)
        {
            while (_isBusyNow == false) {
                _isBusyNow = true;
                var app = Directory.GetCurrentDirectory();
                var entityPath = $"{app}/wwwroot/app/storage/{model.entityName}.json";
                var json = System.IO.File.ReadAllText(entityPath);
                var objects = JsonConvert.DeserializeObject<List<dynamic>>(json);

                var newId = objects.Select(s => s["Id"])?.Max() + 1 ?? 1;

                model.newObj.Id = newId;
                objects.Add(model.newObj);

                var serialized = JsonConvert.SerializeObject(objects, Formatting.Indented);
                System.IO.File.WriteAllText(entityPath, serialized);
                
                _isBusyNow = false;
                break;
            }

            return model.newObj;
        }


        public static dynamic Edit(Edit model)
        {
            dynamic result = null;

            while (_isBusyNow == false)
            {
                var app = Directory.GetCurrentDirectory();
                var entityPath = $"{app}/wwwroot/app/storage/{model.entityName}.json";

                var json = System.IO.File.ReadAllText(entityPath);

                var objects = JsonConvert.DeserializeObject<List<dynamic>>(json);

                foreach (var obj in objects)
                {
                    var id = obj["Id"];

                    if (id == model.id)
                    {
                        result = obj;
                        foreach (var item in model.newObj)
                        {
                            if (obj[item.Name] != null && !string.IsNullOrEmpty(obj[item.Name].ToString()))
                            {
                                obj[item.Name] = item.Value;
                            }
                            else
                            {
                                obj[item.Name] = item.Value;
                            }
                        }
                    }
                }
                var serialized = JsonConvert.SerializeObject(objects, Formatting.Indented);
                System.IO.File.WriteAllText(entityPath, serialized);

                _isBusyNow = false;
                break;
            }

            return result;
        }
    }
}
